<?php
/**
 *
 *
 */
namespace Library;

class Data {

    private $clientId;

    private $clientSecret;

    private $redirectUri;

    private $accessToken;

    private $expireIn;

   public function setClientId($clientId) {
      $this->clientId = $clientId;
   }

   public function getClientId() {
      return $this->clientId;
   }

   public function setClientSecret($clientSecret) {
      $this->clientSecret = $clientSecret;
   }

   public function getClientSecret() {
      return $this->clientSecret;
   }

   public function setRedirectUri($redirectUri) {
      $this->redirectUri = $redirectUri;
   }

   public function getRedirectUri() {
      return $this->redirectUri;
   }
   
   public function setAccessToken($accessToken) {
      $this->accessToken = $accessToken;
   }

   public function getAccessToken() {
      return $this->accessToken;
   }
   
   public function setExpireIn($expireIn) {
      $this->expireIn = $expireIn;
   }

   public function getExpireIn() {
      return $this->expireIn;
   }
}
