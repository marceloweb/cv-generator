<?php

error_reporting(E_ALL);
ini_set('display_errors', true);

require_once "./Autoloader.php";

try {
    $class = Autoloader::loader();
} catch (Exception $e) {
    echo $e->getMessage(), "\n";
}

spl_autoload_register('Autoloader::loader');

$frontController = new \FrontController\FrontController();
$frontController->run();
